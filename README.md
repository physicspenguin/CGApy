[![pipeline status](https://gitlab.com/physicspenguin/CGApy/badges/development/pipeline.svg)](https://gitlab.com/physicspenguin/CGApy/-/commits/development)

# cga_py

cga_py is an implementation of conformal geometric algebra (CGA) for python.

Currently still in heavy development.

## Install
```
pip install cga-py
```

## Usage
See tutorial.py
