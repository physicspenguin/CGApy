# To Do until Package is complete

- [ ] Fully document
	- [x] base_objects.py
	- [ ] conditions.py
	- [x] four_quaternions.py
	- [ ] geom_generators.py
	- [ ] multivector.py
	- [ ] operators.py
	- [ ] permutations.py
	- [ ] random.py
- [ ] Maybe document helper functions
- [ ] Write README
- [ ] Write User Guide
- [ ] implement sphinx
